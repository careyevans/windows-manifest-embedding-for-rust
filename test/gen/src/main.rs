use std::env;

use embed_manifest::{embed_manifest, new_manifest};

fn main() {
    env::set_var("OUT_DIR", ".");
    env::set_var("TARGET", "x86_64-pc-windows-gnu");
    let manifest = new_manifest("O.OU.Name");
    embed_manifest(manifest).unwrap();
}
