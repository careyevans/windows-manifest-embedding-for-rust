#![windows_subsystem = "windows"]

use std::ffi::c_void;
use std::ptr::null;

#[link(name = "kernel32")]
extern "system" {
    fn GetVersion() -> u32;
}

#[link(name = "user32")]
extern "system" {
    fn MessageBoxA(hwnd: *const c_void, lpText: *const u8, lpCaption: *const u8, uType: u32) -> i32;
}

fn main() {
    let version = unsafe { GetVersion() };
    let major_version = version & 0x0f;
    let minor_version = (version & 0xf0) >> 8;
    let build = if version < 0x80000000 { version >> 16 } else { 0 };
    let message = format!(
        "It’s raining 🐈s and 🐕s.\n\nI think this is Windows {}.{} ({}).\0",
        major_version, minor_version, build
    );
    unsafe {
        MessageBoxA(null(), message.as_ptr(), "Manifest Test\0".as_ptr(), 0x40);
    }
}
